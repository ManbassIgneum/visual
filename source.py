import random
import pylab
from matplotlib.widgets import Button
from matplotlib.widgets import TextBox
from matplotlib.widgets import CheckButtons


numof = 0
latest = None
pos = 0
mass = []
step = False
history = []
choosed = [None]
arrx = []
arry = []
ch_file = open("cache.txt", 'w')
class item :
    def __init__(self, a, c, d) :
        self.key = a
        self.prior = random.random() * 1000000000
        self.x = c
        self.y = d
        self.left = None
        self.right = None
        self.l = None
        self.r = None
        self.use = False
        self.found_left = False


class hist:
    def __init__(self, a, b, c):
        self.fr = a
        self.to = b
        self.angl = c


def split(t, key, l, r):
    if (not t[0]):
        l[0] = None
        r[0] = None
    elif (key[0] <= t[0].key):
        lf = [t[0].l]
        split([t[0].l], key, l, lf)
        t[0].l = lf[0]
        r[0] = t[0]
    else:
        lf = [t[0].r]
        split([t[0].r], key, lf, r)
        t[0].r = lf[0]
        l[0] = t[0]


def insert(t, it):
	if (not t[0]):
		t[0] = it[0]
	elif (it[0].prior > t[0].prior):
		lf = [it[0].l]
		rf = [it[0].r]
		split(t, [it[0].key], lf, rf)
		it[0].l = lf[0]
		it[0].r = rf[0]
		t[0] = it[0]
	else:
		if (it[0].key < t[0].key):
			th = [t[0].l]
			insert(th, it)
			t[0].l = th[0]
		else:
			th = [t[0].r]
			insert(th, it)
			t[0].r = th[0]


def merge(t, l, r):
    if (not l[0] or not r[0]):
        if l[0]:
            t[0] = l[0]
        else:
            t[0] = r[0]
    elif (l[0].prior > r[0].prior):
        lf = [l[0].r]
        merge(lf, lf, r)
        l[0].r = lf[0]
        t[0] = l[0]
    else:
        lf = [r[0].l]
        merge(lf, l, lf)
        r[0].l = lf[0]
        t[0] = r[0]


def erase(t, key):
    if (t[0].key == key[0]):
        tr = [t[0].r]
        tl = [t[0].l]
        merge(t, tl, tr)
        t[0].l = tl[0]
        t[0].r = tr[0]
    else:
        if (key[0] < t[0].key):
            tl = [t[0].l]
            erase(tl, key)
            t[0].l = tl[0]
        else:
            tl = [t[0].r]
            erase(tl, key)
            t[0].r = tl[0]


def unite(l, r):
    if (not l[0] or not r[0]):
        if (l[0]):
            return l[0]
        else:
            return r[0]
    if (l[0].prior < r[0].prior):
        emp = l[0]
        l[0] = r[0]
        r[0] = emp
    lt = [None]
    rt = [None]
    split(r, [l[0].key], lt, rt)
    l[0].l = unite(l[0].l, lt)
    l[0].r = unite(l[0].r, rt)
    return l[0]

def ta(a, b, c):
    return (int(b.x) - int(a.x)) * (int(c.y) - int(a.y)) - (int(b.y) - int(a.y)) * (int(c.x) - int(a.x))

def geta(a, b):
    checkerl = ta(a, b, b.left)
    checkerr = ta(a, b, b.right)
    if (((checkerr > 0) == (checkerl > 0)) or
            (checkerr == 0) or (checkerl == 0)):
        return 0
    curta = ta(b.left, b, b.right)
    dotta = ta(b.left, a, b)
    if ((curta >= 0) == (dotta >= 0)):
        return -1
    return 1


def da(a, b, c):
    return (abs(a.x - c.x) < abs(b.x - c.x)) or\
           ((abs(a.x - c.x) == abs(b.x - c.x)) and
            (abs(a.y - c.y) < abs(b.y - c.y)))


def nul(l):
    cur = l
    while (cur.right.use):
        cur = cur.right
        cur.use = False
    cur = l
    while (cur.left.use):
        cur = cur.left
        cur.use = False
    l.use = False


def founding(dot, start):
    global history
    cur = start
    prev = [start, 0]
    prevval = geta(dot, cur)
    l = None
    r = None
    next = None
    used = False
    goleft = False
    prevta = 0
    if prevval == 0:
        history.append(hist(dot, cur, 0))
        l = start
        cur.use = True
        while (geta(dot, cur.left) == 0) and\
                (ta(cur, cur.left, dot) == 0):
            if cur.x == dot.x and cur.y == dot.y:
                if (l):
                    nul(l)
                return False, False
            cur.left.use = True
            cur.left.found_left = True
            if (da(l, cur.left, dot)):
                l = cur.left
            cur = cur.left
            history.append(hist(dot, cur, 0))
        if cur.x == dot.x and cur.y == dot.y:
            if (l):
                nul(l)
            return False, False
    cur = start
    if prevval == 0:
        while (geta(dot, cur.right) == 0) and\
                (ta(dot, cur.right, l) == 0):
            if cur.x == dot.x and cur.y == dot.y:
                if (l):
                    nul(l)
                return False, False
            cur.right.use = True
            cur.right.found_left = False
            if (da(l, cur.right, dot)):
                l = cur.right
            cur = cur.right
            history.append(hist(dot, cur, 0))
        if cur.x == dot.x and cur.y == dot.y:
            if (l):
                nul(l)
            return False, False
        cur = cur.right
        prevval = geta(dot, cur)
    if prevval == 0:
        r = cur
        history.append(hist(dot, cur, 0))
        while (geta(dot, cur.right) == 0) and\
                (ta(cur, cur.right, dot) == 0 and
                    not cur.right.use):
            if cur.x == dot.x and cur.y == dot.y:
                if (l):
                    nul(l)
                if (r):
                    nul(r)
                return False, False
            cur.right.use = True
            if (da(r, cur.right, dot)):
                r = cur.right
            cur = cur.right
            history.append(hist(dot, cur, 0))
        if cur.x == dot.x and cur.y == dot.y:
            if (l):
                nul(l)
            if (r):
                nul(r)
            return False, False
        return l, r
    cur = start
    if not l:
        if cur.r:
            prevta = ta(start, dot, start.right) > 0
            cur = cur.r
            while True:
                val = geta(dot, cur)
                history.append(hist(dot, cur, val))
                if (val == 0):
                    if cur.use:
                        if cur.r and not cur.found_left:
                            cur = cur.r
                        elif (cur.l and cur.found_left):
                            cur = cur.l
                        else:
                            break
                    else:
                        l = cur
                        ccur = cur
                        cur.use = True
                        while (geta(dot, cur.right) == 0)\
                                and (ta(cur, cur.right, dot) == 0):
                            cur.right.use = True
                            cur.right.found_left = False
                            if (da(l, cur.right, dot)):
                                l = cur.right
                            cur = cur.right
                        if (geta(dot, cur.right) == prevval)\
                                or cur.right.use:
                            goleft = True
                        cur = ccur
                        while (geta(dot, cur.left) == 0)\
                                and (ta(cur, cur.left, dot) == 0):
                            cur.left.use = True
                            cur.left.found_left = True
                            if (da(l, cur.left, dot)):
                                l = cur.left
                            cur = cur.left
                        cur = ccur
                        prevval *= -1
                        break
                elif (val != prevval or (cur.l and
                                         ((ta(start, dot, cur) > 0) !=
                                          prevta) and val == prevval)):
                    if (val != prevval and not next):
                        next = cur
                    if cur.l:
                        cur = cur.l
                    else:
                        break
                else:
                    if cur.r:
                        cur = cur.r
                    else:
                        break
    if l:
        if next:
            cur = next
        if goleft:
            if cur.l:
                cur = cur.l
                while True:
                    val = geta(dot, cur)
                    history.append(hist(dot, cur, val))
                    if (val == 0):
                        if cur.use:
                            if cur.r and not cur.found_left:
                                cur = cur.r
                            elif (cur.l and cur.found_left):
                                cur = cur.l
                            else:
                                break
                        else:
                            r = cur
                            ccur = cur
                            cur.use = True
                            while (geta(dot, cur.right) == 0) and\
                                    (ta(cur, cur.right, dot) == 0):
                                cur.right.use = True
                                cur.right.found_left = False
                                if (da(r, cur.right, dot)):
                                    r = cur.right
                                cur = cur.right
                            cur = ccur
                            while (geta(dot, cur.left) == 0) and\
                                    (ta(cur, cur.left, dot) == 0):
                                cur.left.use = True
                                cur.left.found_left = True
                                if (da(r, cur.left, dot)):
                                    r = cur.left
                                cur = cur.left
                            cur = ccur
                            break
                    elif (val == prevval):
                        if cur.l:
                            cur = cur.l
                        else:
                            break
                    else:
                        if cur.r:
                            cur = cur.r
                        else:
                            break
        else:
            if cur.r:
                cur = cur.r
                while True:
                    val = geta(dot, cur)
                    history.append(hist(dot, cur, val))
                    if (val == 0):
                        if cur.use:
                            if cur.r and not cur.found_left:
                                cur = cur.r
                            elif (cur.l and cur.found_left):
                                cur = cur.l
                            else:
                                break
                        else:
                            r = cur
                            ccur = cur
                            cur.use = True
                            while (geta(dot, cur.right) == 0) and\
                                    (ta(cur, cur.right, dot) == 0):
                                cur.right.use = True
                                cur.right.found_left = False
                                if (da(r, cur.right, dot)):
                                    r = cur.right
                                cur = cur.right
                            cur = ccur
                            while (geta(dot, cur.left) == 0) and\
                                    (ta(cur, cur.left, dot) == 0):
                                cur.left.use = True
                                cur.left.found_left = True
                                if (da(r, cur.left, dot)):
                                    r = cur.left
                                cur = cur.left
                            cur = ccur
                            break
                    elif (val != prevval):
                        if cur.l:
                            cur = cur.l
                        else:
                            break
                    else:
                        if cur.r:
                            cur = cur.r
                        else:
                            break
    if r and l:
        return r, l
    if (not r) and l:
        prevval *= -1
        cur = start
        if cur.l:
            cur = cur.l
            while True:
                val = geta(dot, cur)
                history.append(hist(dot, cur, val))
                if (val == 0):
                    if cur.use:
                        if cur.r and not cur.found_left:
                            cur = cur.r
                        elif (cur.l and cur.found_left):
                            cur = cur.l
                        else:
                            break
                    else:
                        r = cur
                        ccur = cur
                        cur.use = True
                        while (geta(dot, cur.right) == 0) and\
                                (ta(cur, cur.right, dot) == 0):
                            cur.right.use = True
                            cur.right.found_left = False
                            if (da(r, cur.right, dot)):
                                r = cur.right
                            cur = cur.right
                        cur = ccur
                        while (geta(dot, cur.left) == 0) and\
                                (ta(cur, cur.left, dot) == 0):
                            cur.left.use = True
                            cur.left.found_left = True
                            if (da(r, cur.left, dot)):
                                r = cur.left
                            cur = cur.left
                        cur = ccur
                        break
                elif (val != prevval):
                    if cur.r:
                        cur = cur.r
                    else:
                        break
                else:
                    if cur.l:
                        cur = cur.l
                    else:
                        break
    if r and l:
        return r, l
    if (not r) and (not l):
        cur = start
        if not l:
            if cur.l:
                prevta = ta(start, dot, start.left) > 0
                cur = cur.l
                while True:
                    val = geta(dot, cur)
                    history.append(hist(dot, cur, val))
                    if (val == 0):
                        if cur.use:
                            if cur.r and not cur.found_left:
                                cur = cur.r
                            elif (cur.l and cur.found_left):
                                cur = cur.l
                            else:
                                break
                        else:
                            l = cur
                            ccur = cur
                            cur.use = True
                            while (geta(dot, cur.right) == 0) and\
                                    (ta(cur, cur.right, dot) == 0):
                                cur.right.use = True
                                cur.right.found_left = False
                                if (da(l, cur.right, dot)):
                                    l = cur.right
                                cur = cur.right
                            cur = ccur
                            while (geta(dot, cur.left) == 0) and\
                                    (ta(cur, cur.left, dot) == 0):
                                cur.left.use = True
                                cur.left.found_left = True
                                if (da(l, cur.left, dot)):
                                    l = cur.left
                                cur = cur.left
                            if (geta(dot, cur.left) == prevval)\
                                    or cur.left.use:
                                goleft = True
                            cur = ccur
                            break
                    elif (val != prevval or (cur.r and
                                             ((ta(start, dot, cur) > 0) !=
                                              prevta) and (val == prevval))):
                        if (val != prevval and not next):
                            next = cur
                        if cur.r:
                            cur = cur.r
                        else:
                            break
                    else:
                        if cur.l:
                            cur = cur.l
                        else:
                            break
        if not l:
            return None, None
        if l:
            if goleft:
                if cur.r:
                    cur = cur.r
                    while True:
                        val = geta(dot, cur)
                        history.append(hist(dot, cur, val))
                        if (val == 0):
                            if cur.use:
                                if cur.r and not cur.found_left:
                                    cur = cur.r
                                elif (cur.l and cur.found_left):
                                    cur = cur.l
                                else:
                                    break
                            else:
                                r = cur
                                ccur = cur
                                cur.use = True
                                while (geta(dot, cur.right) == 0) and\
                                        (ta(cur, cur.right, dot) == 0):
                                    cur.right.use = True
                                    cur.right.found_left = False
                                    if (da(r, cur.right, dot)):
                                        r = cur.right
                                    cur = cur.right
                                cur = ccur
                                while (geta(dot, cur.left) == 0) and\
                                        (ta(cur, cur.left, dot) == 0):
                                    cur.left.use = True
                                    cur.left.found_left = True
                                    if (da(r, cur.left, dot)):
                                        r = cur.left
                                    cur = cur.left
                                cur = ccur
                                break
                        elif (val == prevval):
                            if cur.l:
                                cur = cur.l
                            else:
                                break
                        else:
                            if cur.r:
                                cur = cur.r
                            else:
                                break
            else:
                if next:
                    cur = next
                if cur.l:
                    cur = cur.l
                    while True:
                        val = geta(dot, cur)
                        history.append(hist(dot, cur, val))
                        if (val == 0):
                            if cur.use:
                                if cur.r and not cur.found_left:
                                    cur = cur.r
                                elif (cur.l and cur.found_left):
                                    cur = cur.l
                                else:
                                    break
                            else:
                                r = cur
                                ccur = cur
                                cur.use = True
                                while (geta(dot, cur.right) == 0) and\
                                        (ta(cur, cur.right, dot) == 0):
                                    cur.right.use = True
                                    cur.right.found_left = False
                                    if (da(r, cur.right, dot)):
                                        r = cur.right
                                    cur = cur.right
                                cur = ccur
                                while (geta(dot, cur.left) == 0) and\
                                        (ta(cur, cur.left, dot) == 0):
                                    cur.left.use = True
                                    cur.left.found_left = True
                                    if (da(r, cur.left, dot)):
                                        r = cur.left
                                    cur = cur.left
                                cur = ccur
                                break
                        elif (val != prevval):
                            if cur.l:
                                cur = cur.l
                            else:
                                break
                        else:
                            if cur.r:
                                cur = cur.r
                            else:
                                break
    if (l and r):
        return r, l
    if (l):
        nul(l)
    if (r):
        nul(r)
    return None, None


def upd(rig):
    if (rig.right.key > rig.key):
        if ((rig.right.key - rig.key) < 1):
            rig.key = rig.right.key
            upd(rig.right)
        else:
            rig.key = (rig.key + rig.right.key) / 2
    else:
        rig.key += 40000000000000


def process(start, dot):
    global startval
    global numof
    global mass
    global history
    history = []
    if numof < 2:
        mass.append(dot)
        numof += 1
        return
    if numof == 2:
        if (ta(mass[0], mass[1], dot) == 0):
            if (mass[1].x > mass[0].x) or ((mass[1].x ==
                                            mass[0].x) and
                                           (mass[1].y > mass[0].y)):
                if (dot.x > mass[1].x) or ((dot.x ==
                                            mass[1].x) and
                                           (dot.y > mass[1].y)):
                    mass[1] = dot
                elif ((dot.x < mass[0].x) or ((dot.x ==
                                               mass[0].x) and
                                              (dot.y < mass[0].y))):
                    mass[0] = dot
                return
            if (dot.x > mass[0].x) or ((dot.x ==
                                        mass[0].x) and (dot.y > mass[0].y)):
                mass[0] = mass[1]
                mass[1] = dot
                return
            if ((dot.x < mass[0].x) or ((dot.x ==
                                        mass[0].x) and (dot.y < mass[0].y))):
                if (dot.x < mass[1].x) or ((dot.x ==
                                            mass[1].x) and
                                           (dot.y < mass[1].y)):
                    mass[1] = dot
        else:
            numof += 1
            if (ta(mass[0], mass[1], dot) > 0):
                start[0] = mass[0]
                mass[1].key = 100000000000000
                insert(start, [mass[1]])
                dot.key = 50000000000000
                insert(start, [dot])
                dot.left = mass[0]
                dot.right = mass[1]
                mass[0].left = mass[1]
                mass[0].right = dot
                mass[1].left = dot
                mass[1].right = mass[0]
            else:
                start[0] = mass[0]
                dot.key = 100000000000000
                insert(start, [dot])
                mass[1].key = 50000000000000
                insert(start, [mass[1]])
                mass[1].left = mass[0]
                mass[1].right = dot
                mass[0].left = dot
                mass[0].right = mass[1]
                dot.left = mass[1]
                dot.right = mass[0]
        return
    lef, rig = founding(dot, start[0])
    if not lef or not rig:
        return
    nul(lef)
    nul(rig)
    if (ta(dot, lef, rig) < 0):
        tmp = lef
        lef = rig
        rig = tmp
    if (lef.key < rig.key):
        lf = [lef]
        mid = [None]
        rg = [rig]
        split(start, [lef.right.key], lf, rg)
        split([rg[0]], [rig.key], mid, rg)
        merge(start, lf, rg)
        if (rig.key - lef.key) > 1:
            now = item((lef.key + rig.key)/2, dot.x, dot.y)
        else:
            now = item(rig.key, dot.x, dot.y)
            upd(rig)
        now.left = lef
        now.right = rig
        lef.right = now
        rig.left = now
        insert(start, [now])
    else:
        lf = [None]
        mid = [None]
        rg = [None]
        split(start, [rig.key], lf, rg)
        if (lef.right.key > lef.key):
            split(rg, [lef.right.key], start, lf)
        else:
            start[0] = rg[0]
        now = None
        if (rig.key == 0):
            now = item(lef.key + 100000000000000, dot.x, dot.y)
        else:
            now = item(0, dot.x, dot.y)
        now.left = lef
        now.right = rig
        lef.right = now
        rig.left = now
        insert(start, [now])
    numof += 1
    cur = start[0]
    return


start = [None]

# Начальные параметры графиков
current_sigma = 0.2
current_mu = 0.0
# Создадим окно с графиком
fig, graph_axes = pylab.subplots()
# Оставим снизу от графика место для виджетов
fig.subplots_adjust(left=0.07, right=0.95, top=0.95, bottom=0.2)
# !!! Создадим ось для кнопки
axes_button_add = pylab.axes([0.8, 0.05, 0.15, 0.075])
oybox = pylab.axes([0.67, 0.05, 0.1, 0.075])
oxbox = pylab.axes([0.54, 0.05, 0.1, 0.075])
chek = pylab.axes([0.0, 0.05, 0.18, 0.075])
leftbutx = pylab.axes([0.23, 0.05, 0.13, 0.075])
rightbutx = pylab.axes([0.36, 0.05, 0.13, 0.075])
# !!! Создадим кнопку
button_add = Button(axes_button_add, 'Добавить')
oy = TextBox(oybox, 'y ')
ox = TextBox(oxbox, 'x ')
check = CheckButtons(chek, ['Пошагово'], [False])
leftbut = Button(leftbutx, 'назад')
rightbut = Button(rightbutx, 'вперед')

def buildall(el):
    choosed[0] = el[0].right.right
    cur = el[0]
    arry = []
    arrx = []
    while (cur.right != el[0]):
        arry.append(cur.y)
        arrx.append(cur.x)
        cur = cur.right
    arry.append(el[0].left.y)
    arrx.append(el[0].left.x)
    arry.append(el[0].y)
    arrx.append(el[0].x)
    return arrx, arry


def onCheckClicked(active):
    ''' !!! Обработчик события при клике по CheckButtons'''
    global grid_visible
    global step
    global use
    step = not step
    use = step


def onButtonAddClicked(event):
    ''' !!! Обработчик события для кнопки "Добавить"'''
    global step
    global use
    global current_sigma
    global current_mu
    global graph_axes
    global start
    global ox
    global oy
    global numof
    global pos
    global mass
    global latest
    global arrx
    global arry
    global ch_file
    pos = -1
    use = step
    if (ox.text and oy.text):
        x = int(ox.text)
        y = int(oy.text)
    else:
        x = round(random.random() * 100)
        y = round(random.random() * 100)
    print(str(x) + ' ' + str(y))
    process(start, item(0, x, y))
    while use:
        pylab.pause(0.1)
    graph_axes.cla()
    if numof > 2:
        arrx, arry = buildall(start)
        graph_axes.plot(arrx, arry, 'go:')
    else:
        if len(mass) == 1:
            graph_axes.plot([mass[0].x], [mass[0].y], 'go:')
        else:
            graph_axes.plot([mass[0].x, mass[1].x], [mass[0].y, mass[1].y], 'go:')


def leftclick(event):
    global pos
    global step
    global history
    global graph_axes
    global latest
    if not step:
        return
    if pos < 0:
        return
    pos -= 1
    graph_axes.cla()
    graph_axes.plot(arrx, arry, 'go:')
    for i in range(pos + 1):
        if (history[i].angl == 0):
            graph_axes.plot([history[i].fr.x, history[i].to.x], [history[i].fr.y, history[i].to.y], 'ro:')
        else:
            graph_axes.plot([history[i].fr.x, history[i].to.x], [history[i].fr.y, history[i].to.y], 'ro')

def rightclick(event):
    global pos
    global step
    global history
    global use
    global graph_axes
    if not step:
        return
    if pos > (len(history) - 2):
        use = False
        return
    pos += 1
    if (history[pos].angl == 0):
        graph_axes.plot([history[pos].fr.x, history[pos].to.x], [history[pos].fr.y, history[pos].to.y], 'ro:')
    else:
        graph_axes.plot([history[pos].fr.x, history[pos].to.x], [history[pos].fr.y, history[pos].to.y], 'ro')


button_add.on_clicked(onButtonAddClicked)
leftbut.on_clicked(leftclick)
rightbut.on_clicked(rightclick)
check.on_clicked(onCheckClicked)
# Добавить график

pylab.show()
ch_file.close()
exit(0)