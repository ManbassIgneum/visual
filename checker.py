import random
from time import time


startval = 1000000000.0000
numof = 0
latest = None
pos = 0
mass = []
step = False
choosed = [None]
arrx = []
arry = []


class Dot:
    x = int()
    y = int()
    def __init__(self, fir, sec):
        self.x = fir
        self.y = sec


class item:
    def __init__(self, a, c, d):
        self.key = a
        self.prior = random.random() * 100000000000
        self.x = c
        self.y = d
        self.left = None
        self.right = None
        self.l = None
        self.r = None
        self.use = False
        self.found_left = False


def split(t, key, l, r):
    if (not t[0]):
        l[0] = None
        r[0] = None
    elif (key[0] <= t[0].key):
        lf = [t[0].l]
        split([t[0].l], key, l, lf)
        t[0].l = lf[0]
        r[0] = t[0]
    else:
        lf = [t[0].r]
        split([t[0].r], key, lf, r)
        t[0].r = lf[0]
        l[0] = t[0]


def insert(t, it):
    if (not t[0]):
        t[0] = it[0]
    elif (it[0].prior > t[0].prior):
        lf = [it[0].l]
        rf = [it[0].r]
        split(t, [it[0].key], lf, rf)
        it[0].l = lf[0]
        it[0].r = rf[0]
        t[0] = it[0]
    else:
        if (it[0].key < t[0].key):
            th = [t[0].l]
            insert(th, it)
            t[0].l = th[0]
        else:
            th = [t[0].r]
            insert(th, it)
            t[0].r = th[0]


def merge(t, l, r):
    if (not l[0] or not r[0]):
        if l[0]:
            t[0] = l[0]
        else:
            t[0] = r[0]
    elif (l[0].prior > r[0].prior):
        lf = [l[0].r]
        merge(lf, lf, r)
        l[0].r = lf[0]
        t[0] = l[0]
    else:
        lf = [r[0].l]
        merge(lf, l, lf)
        r[0].l = lf[0]
        t[0] = r[0]


def erase(t, key):
    if (t[0].key == key[0]):
        tr = [t[0].r]
        tl = [t[0].l]
        merge(t, tl, tr)
        t[0].l = tl[0]
        t[0].r = tr[0]
    else:
        if (key[0] < t[0].key):
            tl = [t[0].l]
            erase(tl, key)
            t[0].l = tl[0]
        else:
            tl = [t[0].r]
            erase(tl, key)
            t[0].r = tl[0]


def unite(l, r):
    if (not l[0] or not r[0]):
        if (l[0]):
            return l[0]
        else:
            return r[0]
    if (l[0].prior < r[0].prior):
        emp = l[0]
        l[0] = r[0]
        r[0] = emp
    lt = [None]
    rt = [None]
    split(r, [l[0].key], lt, rt)
    l[0].l = unite(l[0].l, lt)
    l[0].r = unite(l[0].r, rt)
    return l[0]


def ta(a, b, c):
    return (int(b.x) - int(a.x)) * (int(c.y) - int(a.y))\
           - (int(b.y) - int(a.y)) * (int(c.x) - int(a.x))


def cw(fir, sec, thr):
    return ta(fir, sec, thr) < 0


def ccw(fir, sec, thr):
    return ta(fir, sec, thr) > 0


def geta(a, b):
    checkerl = ta(a, b, b.left)
    checkerr = ta(a, b, b.right)
    if (((checkerr > 0) == (checkerl > 0)) or
            (checkerr == 0) or (checkerl == 0)):
        return 0
    curta = ta(b.left, b, b.right)
    dotta = ta(b.left, a, b)
    if ((curta >= 0) == (dotta >= 0)):
        return -1
    return 1


def getaa(a, b, c):
    return (ta(a, b, c) > 0)


def da(a, b, c):
    return (abs(a.x - c.x) < abs(b.x - c.x)) or\
           ((abs(a.x - c.x) == abs(b.x - c.x)) and
            (abs(a.y - c.y) < abs(b.y - c.y)))


def nul(l):
    cur = l
    while (cur.right.use):
        cur = cur.right
        cur.use = False
    cur = l
    while (cur.left.use):
        cur = cur.left
        cur.use = False
    l.use = False


def founding(dot, start):
    cur = start
    prev = [start, 0]
    prevval = geta(dot, cur)
    l = None
    r = None
    next = None
    used = False
    goleft = False
    prevta = 0
    if prevval == 0:
        l = start
        cur.use = True
        while (geta(dot, cur.left) == 0) and\
                (ta(cur, cur.left, dot) == 0):
            if cur.x == dot.x and cur.y == dot.y:
                if (l):
                    nul(l)
                return False, False
            cur.left.use = True
            cur.left.found_left = True
            if (da(l, cur.left, dot)):
                l = cur.left
            cur = cur.left
        if cur.x == dot.x and cur.y == dot.y:
            if (l):
                nul(l)
            return False, False
    cur = start
    if prevval == 0:
        while (geta(dot, cur.right) == 0) and\
                (ta(dot, cur.right, l) == 0):
            if cur.x == dot.x and cur.y == dot.y:
                if (l):
                    nul(l)
                return False, False
            cur.right.use = True
            cur.right.found_left = False
            if (da(l, cur.right, dot)):
                l = cur.right
            cur = cur.right
        if cur.x == dot.x and cur.y == dot.y:
            if (l):
                nul(l)
            return False, False
        cur = cur.right
        prevval = geta(dot, cur)
    if prevval == 0:
        r = cur
        while (geta(dot, cur.right) == 0) and\
                (ta(cur, cur.right, dot) == 0 and\
                         not cur.right.use):
            if cur.x == dot.x and cur.y == dot.y:
                if (l):
                    nul(l)
                if (r):
                    nul(r)
                return False, False
            cur.right.use = True
            if (da(r, cur.right, dot)):
                r = cur.right
            cur = cur.right
        if cur.x == dot.x and cur.y == dot.y:
            if (l):
                nul(l)
            if (r):
                nul(r)
            return False, False
        return l, r
    cur = start
    if not l:
        if cur.r:
            prevta = ta(start, dot, start.right) > 0
            cur = cur.r
            while True:
                val = geta(dot, cur)
                if (val == 0):
                    if cur.use:
                        if cur.r and not cur.found_left:
                            cur = cur.r
                        elif (cur.l and cur.found_left):
                            cur = cur.l
                        else:
                            break
                    else:
                        l = cur
                        ccur = cur
                        cur.use = True
                        while (geta(dot, cur.right) == 0)\
                                and (ta(cur, cur.right, dot) == 0):
                            cur.right.use = True
                            cur.right.found_left = False
                            if (da(l, cur.right, dot)):
                                l = cur.right
                            cur = cur.right
                        if (geta(dot, cur.right) == prevval)\
                                or cur.right.use:
                            goleft = True
                        cur = ccur
                        while (geta(dot, cur.left) == 0)\
                                and (ta(cur, cur.left, dot) == 0):
                            cur.left.use = True
                            cur.left.found_left = True
                            if (da(l, cur.left, dot)):
                                l = cur.left
                            cur = cur.left
                        cur = ccur
                        prevval *= -1
                        break
                elif (val != prevval or (cur.l and
                                         ((ta(start, dot, cur) > 0) !=
                                          prevta) and val == prevval)):
                    if (val != prevval and not next):
                        next = cur
                    if cur.l:
                        cur = cur.l
                    else:
                        break
                else:
                    if cur.r:
                        cur = cur.r
                    else:
                        break
    if l:
        if next:
            cur = next
        if goleft:
            if cur.l:
                cur = cur.l
                while True:
                    val = geta(dot, cur)
                    if (val == 0):
                        if cur.use:
                            if cur.r and not cur.found_left:
                                cur = cur.r
                            elif (cur.l and cur.found_left):
                                cur = cur.l
                            else:
                                break
                        else:
                            r = cur
                            ccur = cur
                            cur.use = True
                            while (geta(dot, cur.right) == 0) and\
                                    (ta(cur, cur.right, dot) == 0):
                                cur.right.use = True
                                cur.right.found_left = False
                                if (da(r, cur.right, dot)):
                                    r = cur.right
                                cur = cur.right
                            cur = ccur
                            while (geta(dot, cur.left) == 0) and\
                                    (ta(cur, cur.left, dot) == 0):
                                cur.left.use = True
                                cur.left.found_left = True
                                if (da(r, cur.left, dot)):
                                    r = cur.left
                                cur = cur.left
                            cur = ccur
                            break
                    elif (val == prevval):
                        if cur.l:
                            cur = cur.l
                        else:
                            break
                    else:
                        if cur.r:
                            cur = cur.r
                        else:
                            break
        else:
            if cur.r:
                cur = cur.r
                while True:
                    val = geta(dot, cur)
                    if (val == 0):
                        if cur.use:
                            if cur.r and not cur.found_left:
                                cur = cur.r
                            elif (cur.l and cur.found_left):
                                cur = cur.l
                            else:
                                break
                        else:
                            r = cur
                            ccur = cur
                            cur.use = True
                            while (geta(dot, cur.right) == 0) and\
                                    (ta(cur, cur.right, dot) == 0):
                                cur.right.use = True
                                cur.right.found_left = False
                                if (da(r, cur.right, dot)):
                                    r = cur.right
                                cur = cur.right
                            cur = ccur
                            while (geta(dot, cur.left) == 0) and\
                                    (ta(cur, cur.left, dot) == 0):
                                cur.left.use = True
                                cur.left.found_left = True
                                if (da(r, cur.left, dot)):
                                    r = cur.left
                                cur = cur.left
                            cur = ccur
                            break
                    elif (val != prevval):
                        if cur.l:
                            cur = cur.l
                        else:
                            break
                    else:
                        if cur.r:
                            cur = cur.r
                        else:
                            break
    if r and l:
        return r, l
    if (not r) and l:
        prevval *= -1
        cur = start
        if cur.l:
            cur = cur.l
            while True:
                val = geta(dot, cur)
                if (val == 0):
                    if cur.use:
                        if cur.r and not cur.found_left:
                            cur = cur.r
                        elif (cur.l and cur.found_left):
                            cur = cur.l
                        else:
                            break
                    else:
                        r = cur
                        ccur = cur
                        cur.use = True
                        while (geta(dot, cur.right) == 0) and\
                                (ta(cur, cur.right, dot) == 0):
                            cur.right.use = True
                            cur.right.found_left = False
                            if (da(r, cur.right, dot)):
                                r = cur.right
                            cur = cur.right
                        cur = ccur
                        while (geta(dot, cur.left) == 0) and\
                                (ta(cur, cur.left, dot) == 0):
                            cur.left.use = True
                            cur.left.found_left = True
                            if (da(r, cur.left, dot)):
                                r = cur.left
                            cur = cur.left
                        cur = ccur
                        break
                elif (val != prevval):
                    if cur.r:
                        cur = cur.r
                    else:
                        break
                else:
                    if cur.l:
                        cur = cur.l
                    else:
                        break
    if r and l:
        return r, l
    if (not r) and (not l):
        cur = start
        if not l:
            if cur.l:
                prevta = ta(start, dot, start.left) > 0
                cur = cur.l
                while True:
                    val = geta(dot, cur)
                    if (val == 0):
                        if cur.use:
                            if cur.r and not cur.found_left:
                                cur = cur.r
                            elif (cur.l and cur.found_left):
                                cur = cur.l
                            else:
                                break
                        else:
                            l = cur
                            ccur = cur
                            cur.use = True
                            while (geta(dot, cur.right) == 0) and\
                                    (ta(cur, cur.right, dot) == 0):
                                cur.right.use = True
                                cur.right.found_left = False
                                if (da(l, cur.right, dot)):
                                    l = cur.right
                                cur = cur.right
                            cur = ccur
                            while (geta(dot, cur.left) == 0) and\
                                    (ta(cur, cur.left, dot) == 0):
                                cur.left.use = True
                                cur.left.found_left = True
                                if (da(l, cur.left, dot)):
                                    l = cur.left
                                cur = cur.left
                            if (geta(dot, cur.left) == prevval)\
                                    or cur.left.use:
                                goleft = True
                            cur = ccur
                            break
                    elif (val != prevval or (cur.r and
                                             ((ta(start, dot, cur) > 0) !=
                                              prevta) and (val == prevval))):
                        if (val != prevval and not next):
                            next = cur
                        if cur.r:
                            cur = cur.r
                        else:
                            break
                    else:
                        if cur.l:
                            cur = cur.l
                        else:
                            break
        if not l:
            return None, None
        if l:
            if goleft:
                if cur.r:
                    cur = cur.r
                    while True:
                        val = geta(dot, cur)
                        if (val == 0):
                            if cur.use:
                                if cur.r and not cur.found_left:
                                    cur = cur.r
                                elif (cur.l and cur.found_left):
                                    cur = cur.l
                                else:
                                    break
                            else:
                                r = cur
                                ccur = cur
                                cur.use = True
                                while (geta(dot, cur.right) == 0) and\
                                        (ta(cur, cur.right, dot) == 0):
                                    cur.right.use = True
                                    cur.right.found_left = False
                                    if (da(r, cur.right, dot)):
                                        r = cur.right
                                    cur = cur.right
                                cur = ccur
                                while (geta(dot, cur.left) == 0) and\
                                        (ta(cur, cur.left, dot) == 0):
                                    cur.left.use = True
                                    cur.left.found_left = True
                                    if (da(r, cur.left, dot)):
                                        r = cur.left
                                    cur = cur.left
                                cur = ccur
                                break
                        elif (val == prevval):
                            if cur.l:
                                cur = cur.l
                            else:
                                break
                        else:
                            if cur.r:
                                cur = cur.r
                            else:
                                break
            else:
                if next:
                    cur = next
                if cur.l:
                    cur = cur.l
                    while True:
                        val = geta(dot, cur)
                        if (val == 0):
                            if cur.use:
                                if cur.r and not cur.found_left:
                                    cur = cur.r
                                elif (cur.l and cur.found_left):
                                    cur = cur.l
                                else:
                                    break
                            else:
                                r = cur
                                ccur = cur
                                cur.use = True
                                while (geta(dot, cur.right) == 0) and\
                                        (ta(cur, cur.right, dot) == 0):
                                    cur.right.use = True
                                    cur.right.found_left = False
                                    if (da(r, cur.right, dot)):
                                        r = cur.right
                                    cur = cur.right
                                cur = ccur
                                while (geta(dot, cur.left) == 0) and\
                                        (ta(cur, cur.left, dot) == 0):
                                    cur.left.use = True
                                    cur.left.found_left = True
                                    if (da(r, cur.left, dot)):
                                        r = cur.left
                                    cur = cur.left
                                cur = ccur
                                break
                        elif (val != prevval):
                            if cur.l:
                                cur = cur.l
                            else:
                                break
                        else:
                            if cur.r:
                                cur = cur.r
                            else:
                                break
    if (l and r):
        return r, l
    if (l):
        nul(l)
    if (r):
        nul(r)
    return None, None


def upd(rig):
    if (rig.right.key > rig.key):
        if ((rig.right.key - rig.key) < 1):
            rig.key = rig.right.key
            upd(rig.right)
        else:
            rig.key = (rig.key + rig.right.key) / 2
    else:
        rig.key += 40000000000000


def process(start, dot):
    global startval
    global numof
    global mass
    if numof < 2:
        mass.append(dot)
        numof += 1
        return
    if numof == 2:
        if (ta(mass[0], mass[1], dot) == 0):
            if (mass[1].x > mass[0].x) or ((mass[1].x ==
                                            mass[0].x) and
                                           (mass[1].y > mass[0].y)):
                if (dot.x > mass[1].x) or ((dot.x ==
                                            mass[1].x) and
                                           (dot.y > mass[1].y)):
                    mass[1] = dot
                elif ((dot.x < mass[0].x) or ((dot.x ==
                                               mass[0].x) and
                                              (dot.y < mass[0].y))):
                    mass[0] = dot
                return
            if (dot.x > mass[0].x) or ((dot.x ==
                                        mass[0].x) and (dot.y > mass[0].y)):
                mass[0] = mass[1]
                mass[1] = dot
                return
            if ((dot.x < mass[0].x) or ((dot.x ==
                                        mass[0].x) and (dot.y < mass[0].y))):
                if (dot.x < mass[1].x) or ((dot.x ==
                                            mass[1].x) and
                                           (dot.y < mass[1].y)):
                    mass[1] = dot
        else:
            numof += 1
            if (ta(mass[0], mass[1], dot) > 0):
                start[0] = mass[0]
                mass[1].key = 100000000000000
                insert(start, [mass[1]])
                dot.key = 50000000000000
                insert(start, [dot])
                dot.left = mass[0]
                dot.right = mass[1]
                mass[0].left = mass[1]
                mass[0].right = dot
                mass[1].left = dot
                mass[1].right = mass[0]
            else:
                start[0] = mass[0]
                dot.key = 100000000000000
                insert(start, [dot])
                mass[1].key = 50000000000000
                insert(start, [mass[1]])
                mass[1].left = mass[0]
                mass[1].right = dot
                mass[0].left = dot
                mass[0].right = mass[1]
                dot.left = mass[1]
                dot.right = mass[0]
        return
    lef, rig = founding(dot, start[0])
    if not lef or not rig:
        return
    nul(lef)
    nul(rig)
    if (ta(dot, lef, rig) < 0):
        tmp = lef
        lef = rig
        rig = tmp
    if (lef.key < rig.key):
        lf = [lef]
        mid = [None]
        rg = [rig]
        split(start, [lef.right.key], lf, rg)
        split([rg[0]], [rig.key], mid, rg)
        merge(start, lf, rg)
        if (rig.key - lef.key) > 1:
            now = item((lef.key + rig.key)/2, dot.x, dot.y)
        else:
            now = item(rig.key, dot.x, dot.y)
            upd(rig)
        now.left = lef
        now.right = rig
        lef.right = now
        rig.left = now
        insert(start, [now])
    else:
        lf = [None]
        mid = [None]
        rg = [None]
        split(start, [rig.key], lf, rg)
        if (lef.right.key > lef.key):
            split(rg, [lef.right.key], start, lf)
        else:
            start[0] = rg[0]
        now = None
        if (rig.key == 0):
            now = item(lef.key + 100000000000000, dot.x, dot.y)
        else:
            now = item(0, dot.x, dot.y)
        now.left = lef
        now.right = rig
        lef.right = now
        rig.left = now
        insert(start, [now])
    numof += 1
    startval /= 2
    cur = start[0]
    return


def update_cache(start):
    global ch_file
    if (start.l and start.r):
        ch_file.write('l: ' + update_cache(start.l) + '     r: ' +  update_cache(start.r) + '\n')
    elif start.l:
        ch_file.write('l: ' + update_cache(start.l) + '    r: None\n')
    elif start.r:
        ch_file.write('l: None     r: ' + update_cache(start.r) + '\n')
    return(str(start.x) + ' ' + str(start.y) + ' ' + str(start.prior) + ' ' + str(start.key))


tim = 0
ii = 0
# num of iteration
while(ii < 100):
    ii += 1
    startval = 1000000000.0000
    numof = 0
    latest = None
    pos = 0
    mass = []
    step = False
    choosed = [None]
    arrx = []
    arry = []
    startt = [None]
    fin = open("input.txt", 'r')
    data = open("data.txt", 'w')
    fout = open("output.txt", 'w')
    gout = open("output2.txt", 'w')
    ch_file = open("cache.txt", 'w')

    # num - num of points
    num = 100000
    #num = int(fin.readline())
    All = []
    bot = []
    top = []
    for i in range(num):
        # comment/uncomment for choose random/from file
        a = random.random() * 2000000000 - 1000000000
        b = random.random() * 2000000000 - 1000000000
        #a, b = fin.readline().split()
        a = int(a)
        b = int(b)
        data.write(str(a) + ' ' + str(b) + '\n')
        tic = time()
        process(startt, item(0, a, b))
        tac = time()
        tim += (tac - tic)
        if (startt[0]):
            ch_file.write('Next\n' + str(a) + ' ' + str(b) + '\n' + 'start: ' + update_cache(startt[0]) + '\n')
        All.append(Dot(a, b))
    All.sort(key=lambda j: (int(j.x), int(j.y)))
    start = All[0]
    end = All[len(All) - 1]
    top.append(start)
    bot.append(start)
    for i in range(int(num)):
        cur = ta(start, All[i], end)
        if i == len(All) - 1 or cur < 0:
            j = len(top) - 1
            while j >= 0:
                if len(top) < 2 or cw(top[j - 1], top[j], All[i]):
                    top.append(All[i])
                    j = 0
                else:
                    top.pop()
                j -= 1
        if i == len(All) - 1 or cur > 0:
            j = len(bot) - 1
            while j >= 0:
                if len(bot) < 2 or ccw(bot[j - 1], bot[j], All[i]):
                    bot.append(All[i])
                    j = 0
                else:
                    bot.pop()
                j -= 1
    Ball = []
    for i in range(len(top)):
        Ball.append(top[i])
    j = len(bot) - 2
    while j > 0:
        Ball.append(bot[j])
        j -= 1
    gout.write(str(len(Ball)) + '\n')
    for i in range(len(Ball)):
        gout.write(str(Ball[i].x) + ' ' + str(Ball[i].y) + '\n')

    cur = startt[0]
    answ = []
    numo = 0
    j = 0
    while ((cur.left.x != Ball[0].x or cur.left.y !=  Ball[0].y) and j <= len(Ball)):
        j += 1
        cur = cur.left
    cur = cur.left
    if (cur.left.x == Ball[1].x and cur.left.y == Ball[1].y):
        sta = cur
        while(cur.left != sta):
            answ.append(cur)
            cur = cur.left
            numo += 1
        answ.append(cur)
        numo += 1
    elif (cur.right.x == Ball[1].x and cur.right.y == Ball[1].y):
        sta = cur
        while (cur.right != sta):
            answ.append(cur)
            cur = cur.right
            numo += 1
        answ.append(cur)
        numo += 1

    fout.write(str(numo) + '\n')
    for el in answ:
        fout.write(str(el.x) + ' ' + str(el.y) + '\n')


    if numo != len(Ball):
        print('No')
        break
    for i in range(numo):
        if answ[i].x != Ball[i].x or answ[i].y != Ball[i].y:
            print('No')
            break
    fin.close()
    fout.close()
    gout.close()
    ch_file.close()
    data.close()

tim /= ii
print('avg time: ' + str(tim))
exit(0)